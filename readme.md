# Setup
- Create a simple test to see that phpunit is working (you'll find a file within the `/tests` directory to get you started.)
- Add PSR-4 autoloading
- Add a class to see that PSR-4 is working
- Commit your changes as you work!


# Overview
We are creating an application to represent an typical Advocacy model. Within this model, we have the concept of the following:

- Clients (usually some type of organization)
- Advocates (an individual who solicits donations on behalf of an organization)
- Donations (monetary contributions to an organization)


## More details

### Clients
- Clients can have many advocates, but there should be a maximum number allowed.
- The maximum number allowed is assigned when a Client is created.
- If the the maximum number of Advocates have been reached for a particular Client and another Advocate is attempted to be associated with that Client, an Exception is thrown.
- A Client should also be given a name upon creation.

### Advocates
- Advocates are always associated with a client.
- Advocates can be a part of 1 and only 1 client.
- An Advocate should always have: `first_name`, `last_name`, `email`, `notified DEFAULT 0` (have they been notified after having been associated with a client?).

### Donations
- Advocates can receive Donations, but only if they've been notified
- If a Donation is attempted to be given to an Advocate who has NOT been notified, an Exception should be thrown.
- We need a way to see the total number and sum (dollar amount) of Donations for each Advocate.
- We need a way to see the total number and sum (dollar amount) of Donations for ALL Advocates within a single Client.


## Finally...

In our system, an Advocate can take on different forms. One specific (concrete) type represents an alumni of a college or university. Let's call this type of Advocate a "Class Agent".

All Advocates have certain things they must do, but Class Agents in particular can be assigned to a Class Year.

Describe (or even attempt to code!) a good way to go about this.